package two;

import one.One;
//import one.Two; // => not accessible from out side package

public class Client {
    public static void main(String[] args) {
        One one = new One();
        one.publicfunc();
//      one.defaultfunc(); //=> absolutely can't access
//      one.privatefunc(); //=> absolutely can't access
//      one.protectedfunc(); //=> can't access directly like this but we can extend
        Child child = new Child();
        child.extendedFunc();
    }
}

class Child extends One{
    void extendedFunc(){
        protectedfunc();
    }
}
/*=> we can access protected function from another package via inheritance*/