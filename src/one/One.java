package one;

public class One {
    public void publicfunc() {
        System.out.println("This is public function.");
    }

    void defaultfunc() {
        System.out.println("This is default function.");
    }

    protected void protectedfunc() {
        System.out.println("This is protected function.");
    }

    private void privatefunc() {
        System.out.println("This is private function.");
    }
}

class Two {
    public void publicfunc() {
        System.out.println("This is public function.");
    }

    void defaultfunc() {
        System.out.println("This is default function.");
    }

    protected void protectedfunc() {
        System.out.println("This is protected function.");
    }

    private void privatefunc() {
        System.out.println("This is private function.");
    }
}

/*public class Three{}*/
/* => In 1 file, have only 1 public class*/

/*class Three{}
class Four{}*/
/* => In 1 file, can have multi default class*/

/*
protected class Five{}
private class Five{}*/
/* => protected and private class are not allowed */